#include "syncQueue.h"
#include "threadPool.h"

void test(int i, int j) {
	cout << "i is " << i << ", j is " << j << endl;
	cout << "this is a test function" << endl;
}

void put(std::function<void()>& task) {
	cout << "lvalue" << endl;
	task();
}

void put(std::function<void()>&& task) {
	cout << "rvalue" << endl;
	task();
}

int main() 
{
	// 仅用于同步队列测试
	/*SyncQueue syn(8);
	for (int i = 0; i < 4; i++) {
		syn.putTask([i] {cout << "i is " << i << endl; });
	}
	
	for (int i = 0; i < 4; i++) {
		syn.putTask(test, i, i+4);
	}

	std::function<void()> task;
	for (int i = 0; i < 4; i++) {
		syn.takeTask(task);
		task();
	}
	for (int i = 0; i < 4; i++) {
		syn.takeTask(task);
		task();
	}*/

	{
		ThreadPool pool(3);
		for (int i = 0; i < 8; i++) {
			pool.addTask([i] {cout << "xxxxx " << endl; });
			//pool.addTask(std::bind(test, i, i + 4));
		}
	}
	
	cout << "ok" << endl;
	getchar();
	return 0;
}