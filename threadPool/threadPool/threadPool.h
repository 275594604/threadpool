#pragma once
#include "syncQueue.h"
#include <vector>
#include <atomic>
#include <chrono>
const int queueSize = 100;
/*
** 扩展 使用future存放线程执行完后的信息， 如一些带返回值的线程
**      可增加内存管理分配， 如内存池
*/
class ThreadPool
{
public:
	using Task = std::function<void()>;
	ThreadPool(int maxThreads = std::thread::hardware_concurrency())
		: mMaxThreads(maxThreads), mTaskQueue(queueSize), isStopping(false) {
		for (int i = 0; i < mMaxThreads; i++) {
			mThreadPool.emplace_back(
				[this] {
				for (;;) {
					Task task;
					{	//cout << "wwwww" << endl;
						std::unique_lock<std::mutex> mLocker(this->mtx);

						this->condit.wait(mLocker, [this] { return this->isStopping || !this->mTaskQueue.isEmpty(); });
						if (isStopping && this->mTaskQueue.isEmpty())
							return;
						this->mTaskQueue.takeTask(task);
					}
					std::this_thread::sleep_for(std::chrono::seconds(1));
					task();
				}
				return;
			}
			);
		}
	}

	// 添加任务
	template<typename F, typename... Args>
	void addTask(F&& f, Args... args) {
		Task t = std::bind(std::forward<F>(f), (std::forward<Args>(args))...);
		mTaskQueue.putTask(std::move(f));
	}

	~ThreadPool() {
		//mTaskQueue.stopSyncQueue();
		{
			std::unique_lock<std::mutex> mLocker(mtx);
			isStopping = true;
		}
		condit.notify_all();
		for (auto& th : mThreadPool)
			th.join();
		mTaskQueue.stopSyncQueue();
	}

private:
	SyncQueue mTaskQueue;                       // 同步队列
	std::vector<std::thread> mThreadPool;       // 线程池
	int mMaxThreads;                            // 最大线程数
	std::condition_variable condit;             // 条件变量
	std::mutex mtx;                             // 互斥量
	bool isStopping;                            // 线程池是否运行
};
