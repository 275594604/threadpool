#pragma once
#include <iostream>
#include <list>                 // std::list
#include <thread>               // std::thread
#include <mutex>                // std::mutex
#include <condition_variable>   // std::condition_variable, std::cv_status::timeout
#include <utility>              // std::move, std::forward
#include <functional>           // std::function, std::bind

using std::cout;
using std::endl;

class SyncQueue
{
public:
	SyncQueue(int maxSize) : mMaxSize(maxSize), isStop(false) {};
	
	// 添加任务
	template<typename F, typename... Args>
	void putTask(F&& f, Args... args) {
		std::unique_lock<std::mutex> mLock(mMutex);
		if (mQueue.size() >= mMaxSize) {
			cout << "缓冲区满了， 请等待..." << endl;
		}
		mNotFull.wait(mLock, [this] { return isStop || (mQueue.size() < mMaxSize); });
		if (isStop) {
			cout << "同步队列停止工作..." << endl;
			return;
		}
		mQueue.push_back( std::bind(std::forward<F>(f), (std::forward<Args>(args))...) );
		mNotEmpty.notify_one();
	}

	/*void putTask(T& t) {
		std::unique_lock<std::mutex> mLock(mMutex);
		if (mQueue.size() >= mMaxSize) {
			cout << "缓冲区满了， 请等待..." << endl;
		}
		mNotFull.wait(mLock, [this] { return isStop || (mQueue.size() < mMaxSize); });
		if (isStop) {
			cout << "同步队列停止工作..." << endl;
			return;
		}
		mQueue.push_back(t);
		mNotEmpty.notify_one();
	}*/

	// 获取任务以执行(一次取出一个任务， 可重载一个一次取出全部任务的takeTask(stdt::list<T>&)函数)
	void takeTask(std::function<void()>& task) {
		std::unique_lock<std::mutex> mLock(mMutex);
		if (mQueue.empty()) {
			cout << "缓冲区空了， 请等待..." << endl;
		}
		mNotEmpty.wait(mLock, [this] { return isStop || !mQueue.empty(); });
		
		// 一定时间内同步队列一直为空则直接停止同步队列
		/*while (!mNotEmpty.wait_for(mLock, std::chrono::seconds(2), [this] { return isStop || !mQueue.empty(); })) {
			cout << "同步队列停止工作..." << endl;
			//isStop = true;
			return;
		}*/

		if (isStop) {
			cout << "同步队列停止工作..." << endl;
			return;
		}

		task = std::move(mQueue.front());
		mQueue.pop_front();
		mNotFull.notify_one();
	}
	
	// 停止同步队列
	void stopSyncQueue() {
		std::unique_lock<std::mutex> mLock(mMutex);
		isStop = true;
		mNotFull.notify_all();
		mNotEmpty.notify_all();
	}
	
	// 同步队列是否为空
	bool isEmpty() {
		std::unique_lock<std::mutex> mLock(mMutex);
		return mQueue.empty();
	}
	
	// 同步队列是否为满
	bool isFull() {
		std::unique_lock<std::mutex> mLock(mMutex);
		return mQueue.size() == mMaxSize;
	}

	// 返回同步队列任务个数
	size_t sizeQueue() {
		std::unique_lock<std::mutex> mLock(mMutex);
		return mQueue.size();
	}

private:
	std::list<std::function<void()>> mQueue;    // 缓冲区队列
	std::mutex mMutex;                          // 互斥量
	int mMaxSize;                               // 同步队列中任务的最大个数
	bool isStop;                                // 同步队列是否在运行
	std::condition_variable mNotFull;           // 不为满的条件变量
	std::condition_variable mNotEmpty;          // 不为空的条件变量
};
